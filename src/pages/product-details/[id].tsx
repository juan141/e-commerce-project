/* eslint-disable react-hooks/rules-of-hooks */
import ProductDetails from "@/components/ProductDetails";
import useGetProductDetails from "@/hooks/useGetProductDetails";
import { IProduct } from "@/interfaces/Product";
import Head from "next/head";
import { useRouter } from "next/router";
import Skeleton from "react-loading-skeleton";

interface Props {
  product: IProduct;
}

function productDetails() {
  const router = useRouter();
  const { id } = router.query;
  const { product, loading } = useGetProductDetails({ id: Number(id) });
  return (
    <>
      {loading ? (
        <Skeleton />
      ) : (
        <>
          {product?.title && (
            <Head>
              <title>{product.title}</title>
            </Head>
          )}
          <ProductDetails
            id={product?.id}
            title={product?.title}
            price={product?.price}
            description={product?.description}
            category={product?.category}
            image={product?.image}
            rating={product?.rating}
          />
        </>
      )}
    </>
  );
}

export default productDetails;
