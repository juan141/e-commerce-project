import Header from "@/components/Header";
import ProductFeed from "@/components/ProductFeed";
import useGetProducts from "@/hooks/useGetProducts";
import Head from "next/head";
import Skeleton from "react-loading-skeleton";
import { useState, useEffect } from "react";
import { IProduct } from "@/interfaces/Product";
import { AiOutlineClose } from "react-icons/ai";
import HeaderMobile from "@/components/HeaderMobile";
import Pagination from "@/components/Pagination";

const Home = () => {
  const [page, setPage] = useState<number>(0);
  const [total, setTotal] = useState<number>(0);
  const { getProducts, products, loading, categories } = useGetProducts({
    page,
    setTotal,
  });
  const [customProducts, setCustomProducts] = useState<IProduct[]>(products);
  const [customCategories, setCustomCategories] =
    useState<string[]>(categories);
  const [customLoading, setCustomLoading] = useState<boolean>(loading);
  const [filterSelected, setFilterSelected] = useState<string>("");

  const headerProps = {
    setCustomProducts,
    setCustomCategories,
    setCustomLoading,
    setFilterSelected,
  };

  useEffect(() => {
    if (!loading) {
      setCustomProducts(products);
      setCustomLoading(loading);
      setCustomCategories(categories);
    }
  }, [loading]);

  return (
    <div className="w-full">
      <Head>
        <title>Home</title>
      </Head>
      <Header {...headerProps} />
      <HeaderMobile {...headerProps} />
      {customLoading ? (
        <Skeleton />
      ) : (
        <>
          {customProducts.length === 0 ? (
            <>
              <div className="w-full">
                <span className="mt-20 text-center flex justify-center items-center ">
                  No items to show
                </span>
              </div>
              {filterSelected.length > 0 && (
                <div className="w-full ml-8 flex items-center gap-2 mb-6">
                  Filter:
                  <div className="flex gap-2 items-center rounded p-2 bg-amber-300">
                    <span>{filterSelected}</span>
                    <AiOutlineClose
                      className="w-2 cursor-pointer text-black"
                      onClick={() => {
                        setFilterSelected("");
                        getProducts();
                      }}
                    />
                  </div>
                </div>
              )}
            </>
          ) : (
            <>
              <ProductFeed
                products={customProducts}
                categories={customCategories}
                filterSelected={filterSelected}
                setFilterSelected={setFilterSelected}
                getProducts={getProducts}
              />
              {filterSelected.length === 0 && (
                <Pagination page={page} setPage={setPage} total={total} />
              )}
            </>
          )}
        </>
      )}
    </div>
  );
};

export default Home;
