import { selectItems, selectTotal, emptyBasket } from "@/slices/basketSlice";
import Head from "next/head";
import { useDispatch, useSelector } from "react-redux";
import Currency from "react-currency-formatter";
import CartProduct from "@/components/CartProduct";
import { BsCartX } from "react-icons/bs";
import { IoMdArrowBack } from "react-icons/io";
import { useRouter } from "next/router";

const Cart = () => {
  const items = useSelector(selectItems);
  const total = useSelector(selectTotal);
  const dispatch = useDispatch();
  const router = useRouter();

  return (
    <>
      <div className="bg-gray-100">
        <IoMdArrowBack
          onClick={() => router.push("/")}
          className="w-20 h-8 pt-2 cursor-pointer"
        />
      </div>

      <Head>
        <title>Cart</title>
      </Head>
      <div className="bg-gray-100 py-10 md:px-6 heightFix">
        <main className="max-w-screen-xl mx-auto">
          {items?.length ? (
            <div className="my-6 shadow rounded-md">
              <div className="flex flex-col md:p-8  p-6  bg-white">
                <h1 className="sm:text-2xl text-xl  font-semibold border-b-2 border-gray-200 pb-4 text-gray-700">
                  Shopping Cart
                </h1>
                <div className="flex justify-between items-center py-6">
                  <span className="font-medium text-lg text-amber-300">
                    Items
                    <span className="font-semibold text-xl ml-2">
                      {items?.length}
                    </span>
                  </span>
                  <button
                    className="button-red py-2 px-8 xs:px-10"
                    onClick={() => dispatch(emptyBasket(items))}
                  >
                    Empty Cart
                  </button>
                </div>
                {items.map((item, i) => (
                  <CartProduct
                    key={`cart-product-${item?.id}`}
                    id={item?.id}
                    title={item?.title}
                    price={item?.price}
                    description={item?.description}
                    category={item?.category}
                    image={item?.image}
                    rating={item?.rating}
                    qty={item?.qty}
                    border={i !== items?.length - 1}
                  />
                ))}
              </div>
            </div>
          ) : (
            <div className="flex items-center justify-center w-full  px-6 lg:py-20 sm:py-10 py-4">
              <div className="text-center md:max-w-none sm:w-auto mx-auto max-w-xs w-4/5">
                <BsCartX className="w-80 h-80" />
                <h3 className="lg:text-2xl text-xl font-medium mt-4">
                  Your Cart is Empty
                </h3>
              </div>
            </div>
          )}
          {!!items?.length && (
            <div className="flex flex-col bg-white md:p-10  py-8 px-6 shadow-md rounded-md md:text-xl sm:text-lg text-base my-10">
              <h2 className="whitespace-nowrap font-semibold overflow-x-auto hideScrollBar">
                Subtotal ({items.length} items) :
                <span className="font-bold text-red-500 mx-2">
                  <Currency quantity={total} currency="USD" />
                </span>
              </h2>
            </div>
          )}
        </main>
      </div>
    </>
  );
};

export default Cart;
