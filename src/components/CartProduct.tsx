import { removeFromBasket, updateQty } from "@/slices/basketSlice";
import { useRouter } from "next/router";
import React from "react";
import { useDispatch } from "react-redux";
import Image from "next/image";
import Link from "next/link";
import Currency from "react-currency-formatter";
import { AiFillMinusCircle, AiFillStar } from "react-icons/ai";
import { AiOutlinePlus } from "react-icons/ai";

interface Props {
  id: number;
  title: string;
  description: string;
  price: number;
  rating: number;
  category: string;
  image: string;
  qty?: number;
  border?: boolean;
  disabled?: boolean;
}

const CartProduct = ({
  id,
  title,
  price,
  description,
  category,
  image,
  qty,
  border,
  disabled,
  rating,
}: Props) => {
  const dispatch = useDispatch();
  const router = useRouter();
  const total = price * qty!;
  const removeItemFromCart = () => dispatch(removeFromBasket({ id }));
  const incQty = () =>
    dispatch(
      updateQty({
        id,
        title,
        price,
        description,
        category,
        image,
        rating,
        qty: qty! + 1,
      })
    );
  const decQty = () =>
    dispatch(
      updateQty({
        id,
        title,
        price,
        description,
        category,
        image,
        rating,
        qty: qty! - 1,
      })
    );
  return (
    <div
      className={`block bg-white py-6 sm:grid sm:grid-cols-5 ${
        border ? "border-b border-gray-300" : ""
      }`}
    >
      <div className="text-center sm:text-left my-auto">
        <Image
          src={image}
          width={150}
          height={150}
          objectFit="contain"
          className="cursor-pointer"
          alt=""
          onClick={() => router.push(`/product-details/${id}`)}
        />
      </div>

      <div className="col-span-3 sm:p-4 mt-2 mb-6 sm:my-0">
        <h4 className="mb-3 link lg:text-xl md:text-lg text-base capitalize font-medium">
          <Link href={`/product-details/${id}`}>{title}</Link>
        </h4>
        <p className="lg:text-sm text-xs my-2  mb-4 line-clamp-3 link text-gray-500">
          <Link href={`/product-details/${id}`}>{description}</Link>
        </p>
        <div className="flex gap-1 items-center">
          <AiFillStar className="text-amber-300" />
          <span>{rating}</span>
        </div>
        <span className="font-medium md:text-base text-sm">
          {qty} × <Currency quantity={price} currency="USD" /> =
          <span className="font-bold text-gray-700 mx-1">
            <Currency quantity={total} currency="USD" />
          </span>
        </span>
      </div>

      <div className="flex flex-col space-y-4 my-auto  justify-self-end">
        <div className="flex justify-between">
          <button
            className={`button sm:p-1 ${disabled ? "opacity-50" : ""}`}
            onClick={decQty}
            data-testid={"minus-button"}
            disabled={disabled}
          >
            <AiFillMinusCircle className="h-5" />
          </button>
          <div className="p-2 whitespace-normal sm:p-1 sm:whitespace-nowrap">
            <span className="font-bold md:text-base text-sm text-gray-700">
              {qty}
            </span>
          </div>
          <button
            data-testid={"plus-button"}
            className={`button sm:p-1 ${disabled ? "opacity-50" : ""}`}
            onClick={incQty}
            disabled={disabled}
          >
            <AiOutlinePlus className="h-5" />
          </button>
        </div>
        <button
          className={`button py-2  lg:px-10 md:px-8 px-6 ${
            disabled ? "opacity-50" : ""
          }`}
          onClick={removeItemFromCart}
          disabled={disabled}
        >
          Remove
        </button>
      </div>
    </div>
  );
};

export default CartProduct;
