import { IProduct } from "@/interfaces/Product";
import React from "react";
import { useState, Dispatch, SetStateAction } from "react";
import Product from "./Product";
import { HiAdjustments } from "react-icons/hi";
import { AiOutlineClose } from "react-icons/ai";

interface Props {
  products: IProduct[];
  categories: string[];
  filterSelected: string;
  setFilterSelected: Dispatch<SetStateAction<string>>;
  getProducts: () => void;
}

const ProductFeed = ({
  products,
  categories,
  filterSelected,
  setFilterSelected,
  getProducts,
}: Props) => {
  const [categoryActive, setCategoryActive] = useState("all");
  const [filteredProducts, setFilteredProducts] = useState(products);
  const activeCategoryHandler = (category: string) => {
    if (category === "all" || categoryActive === category) {
      setCategoryActive("all");
      return;
    }
    setCategoryActive(category);
    filterProducts(category);
  };

  const filterProducts = (category: string) => {
    setFilteredProducts(
      products.filter((product) => product?.category === category)
    );
  };
  return (
    <div className="w-full py-10 px-6 bg-gray-100" id="products-feed">
      <div className="flex items-center w-full max-w-screen-xl sm:mb-8 mb-8 gap-4  mx-auto overflow-x-auto hideScrollBar capitalize text-sm font-medium">
        <div>
          <HiAdjustments className="w-6" />
        </div>
        <div
          className={`${
            categoryActive === "all" ? "bg-gray-500 text-white" : ""
          } py-2 px-6 bg-white text-center rounded hover:bg-amber-300 hover:text-white transition-all cursor-pointer ease-in-out duration-200 shadow`}
          onClick={() => activeCategoryHandler("all")}
        >
          All
        </div>
        {categories?.map((category, i) => (
          <div
            key={`category-${i}`}
            className={`${
              categoryActive === category ? "bg-gray-500 text-white" : ""
            } py-2 px-6 bg-white text-center whitespace-nowrap rounded hover:bg-amber-300 hover:text-white transition-all cursor-pointer ease-in-out duration-200 shadow`}
            onClick={() => activeCategoryHandler(category)}
          >
            {category}
          </div>
        ))}
      </div>
      {filterSelected.length > 0 && (
        <div className="w-full flex items-center gap-2 mb-6">
          Filter:
          <div className="flex gap-2 items-center rounded p-2 bg-amber-300">
            <span>{filterSelected}</span>
            <AiOutlineClose
              className="w-2 cursor-pointer text-black"
              onClick={() => {
                setFilterSelected("");
                getProducts();
              }}
            />
          </div>
        </div>
      )}
      <div className="grid grid-flow-row-dense sm:grid-cols-2 lg:grid-cols-3 xl:grid-cols-4 grid-cols-1 mx-auto max-w-screen-xl gap-x-6 gap-y-8">
        {(categoryActive === "all" ? products : filteredProducts)?.map(
          ({ id, title, price, description, category, image, rating }) => (
            <Product
              key={`product-${id}`}
              id={id}
              title={title}
              price={price}
              description={description}
              category={category}
              image={image}
              rating={rating}
            />
          )
        )}
      </div>
    </div>
  );
};

export default ProductFeed;
