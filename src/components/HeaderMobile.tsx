import { useRouter } from "next/router";
import { useSelector } from "react-redux";
import { selectItems } from "@/slices/basketSlice";
import Search from "./Search";
import { FiMenu, FiShoppingCart } from "react-icons/fi";
import { Dispatch, SetStateAction } from "react";
import { IProduct } from "@/interfaces/Product";

interface Props {
  setCustomProducts: Dispatch<SetStateAction<IProduct[]>>;
  setCustomCategories: Dispatch<SetStateAction<string[]>>;
  setCustomLoading: Dispatch<SetStateAction<boolean>>;
  setFilterSelected: Dispatch<SetStateAction<string>>;
}

function HeaderMobile({
  setCustomProducts,
  setCustomCategories,
  setCustomLoading,
  setFilterSelected,
}: Props) {
  const router = useRouter();
  const items = useSelector(selectItems);

  return (
    <header className="sticky top-0 inset-x-0 z-30 bg-white text-gray-900 glassmorphism px-6 md:hidden block py-4 transition-all">
      <div className="flex items-center w-full justify-between  mb-4">
        <div className="flex items-center space-x-4">
          <div>
            <FiMenu className="w-8" />
          </div>
          <div className="flex items-center">
            <span className="text-amber-300">E-Commerce</span>
          </div>
        </div>
        <div
          data-testid={"cart-icon"}
          className="relative cursor-pointer"
          onClick={() => router.push("/cart")}
        >
          <FiShoppingCart className="xl:w-10 w-9 link" />
          <div
            data-testid={"cart-item-count"}
            className="absolute -top-2 -right-1 rounded-full text-black bg-amber-300 p-1 flex items-center justify-center text-xs font-extrabold"
          >
            {items?.length}
          </div>
        </div>
      </div>
      <div>
        <Search
          {...{
            setFilterSelected,
            setCustomProducts,
            setCustomCategories,
            setCustomLoading,
          }}
        />
      </div>
    </header>
  );
}

export default HeaderMobile;
