import { useRouter } from "next/router";
import { useSelector } from "react-redux";
import { selectItems } from "@/slices/basketSlice";
import Search from "./Search";
import { FiShoppingCart } from "react-icons/fi";
import { Dispatch, SetStateAction } from "react";
import { IProduct } from "@/interfaces/Product";

interface Props {
  setCustomProducts: Dispatch<SetStateAction<IProduct[]>>;
  setCustomCategories: Dispatch<SetStateAction<string[]>>;
  setCustomLoading: Dispatch<SetStateAction<boolean>>;
  setFilterSelected: Dispatch<SetStateAction<string>>;
}

function Header({
  setCustomProducts,
  setCustomCategories,
  setCustomLoading,
  setFilterSelected,
}: Props) {
  const router = useRouter();
  const items = useSelector(selectItems);

  return (
    <header className="sticky top-0 inset-x-0 z-30 bg-white text-gray-900 glassmorphism px-6 md:block hidden">
      <div className="flex items-center w-full max-w-screen-xl py-2 xl:space-x-16 lg:space-x-12  space-x-7  mx-auto">
        <div className="flex items-center h-12">
          <span
            className="text-amber-300 cursor-pointer"
            onClick={() => router.push("/")}
          >
            E-Commerce
          </span>
        </div>
        <div className="flex-grow">
          <Search
            {...{
              setFilterSelected,
              setCustomProducts,
              setCustomCategories,
              setCustomLoading,
            }}
          />
        </div>

        <div
          data-testid={"cart-icon"}
          className="relative cursor-pointer"
          onClick={() => router.push("/cart")}
        >
          <FiShoppingCart className="xl:w-10 lg:w-9 w-8 link mr-1" />
          <div
            data-testid={"cart-item-count"}
            className="absolute -top-2 -right-1 rounded-full text-black bg-amber-300 p-1 flex items-center justify-center text-xs font-extrabold"
          >
            {items?.length}
          </div>
        </div>
      </div>
    </header>
  );
}

export default Header;
