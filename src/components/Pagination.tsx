import { Dispatch, SetStateAction } from "react";

interface Props {
  page: number;
  total: number;
  setPage: Dispatch<SetStateAction<number>>;
}

const Pagination = ({ page, setPage, total }: Props) => {
  const handlePrev = () => {
    if (page <= 0) {
      setPage(0);
    } else {
      setPage((prev) => prev - 1);
    }
  };
  const handleNext = () => {
    if (page >= 9) {
      setPage(0);
    } else {
      setPage((prev) => prev + 1);
    }
  };
  return (
    <div className="bg-gray-100 flex w-full justify-center items-center py-10 gap-2">
      <button
        data-testid={"prev-btn"}
        className="bg-gray-500 rounded p-2 text-white"
        onClick={handlePrev}
      >
        Prev
      </button>
      <span>
        {page + 1} of {total / 10}
      </span>
      <button
        className="bg-gray-500 rounded p-2 text-white"
        onClick={handleNext}
        data-testid={"next-btn"}
      >
        Next
      </button>
    </div>
  );
};

export default Pagination;
