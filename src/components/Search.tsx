import { BiSearch } from "react-icons/bi";
import { useState } from "react";
import { Dispatch, SetStateAction } from "react";
import { IProduct } from "@/interfaces/Product";

interface Props {
  setCustomProducts: Dispatch<SetStateAction<IProduct[]>>;
  setCustomCategories: Dispatch<SetStateAction<string[]>>;
  setCustomLoading: Dispatch<SetStateAction<boolean>>;
  setFilterSelected: Dispatch<SetStateAction<string>>;
}

function Search({
  setCustomProducts,
  setCustomCategories,
  setCustomLoading,
  setFilterSelected,
}: Props) {
  const [search, setSearch] = useState<string>("");
  const handleKeyDown = (event: any) => {
    if (event.key === "Enter" && search.length > 0) {
      setCustomLoading(true);
      fetch(`https://dummyjson.com/products/search?q=${search}`)
        .then((res) => res.json())
        .then((data) => {
          setCustomProducts(
            data.products.map((product: any) => ({
              id: product.id,
              title: product.title,
              description: product.description,
              price: product.price,
              image: product.images[0],
              category: product.category,
              rating: product.rating,
            }))
          );
          const category = data.products.map(
            (product: IProduct) => product.category
          );
          setCustomCategories(
            category.filter(
              (item: string, index: number) => category.indexOf(item) === index
            )
          );
          setCustomLoading(false);
        });
      setFilterSelected(search);
      setSearch("");
    }
  };
  return (
    <div className="relative flex rounded-md  items-center">
      <div className="absolute inset-y-0 left-2.5 flex items-center">
        <BiSearch className="w-4 text-gray-600" />
      </div>
      <input
        className="p-2 pl-10 h-full flex-grow flex-shrink outline-none cursor-pointer sm:text-base text-sm rounded-lg bg-gray-200"
        type="text"
        placeholder="Search a product"
        value={search}
        onChange={(e) => setSearch(e.target.value)}
        onKeyDown={handleKeyDown}
      />
    </div>
  );
}

export default Search;
