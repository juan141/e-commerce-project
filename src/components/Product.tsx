import Image from "next/image";
import React from "react";
import { FiShoppingCart } from "react-icons/fi";
import { AiFillStar } from "react-icons/ai";
import { useRouter } from "next/router";
import Link from "next/link";
import Currency from "react-currency-formatter";
import { useDispatch } from "react-redux";
import { addToBasket } from "@/slices/basketSlice";

interface Props {
  description: string;
  title: string;
  image: string;
  category: string;
  price: number;
  id: number;
  rating: number;
}

const Product = ({
  category,
  description,
  title,
  image,
  price,
  id,
  rating,
}: Props) => {
  const router = useRouter();
  const dispatch = useDispatch();
  const addItemToCart = () => {
    dispatch(
      addToBasket({
        id,
        title,
        price,
        description,
        category,
        image,
        rating,
        qty: 1,
        toast: true,
      })
    );
  };
  return (
    <div className="relative flex flex-col   bg-white z-20  md:p-8 p-6 rounded-md shadow-lg">
      <p className="absolute top-2 right-3 text-xs italic text-gray-400 capitalize">
        {category}
      </p>
      <Image
        src={image}
        height={200}
        width={200}
        alt="product-image"
        objectFit="contain"
        className="cursor-pointer"
        onClick={() => router.push(`/product-details/${id}`)}
      />
      <h4 className="my-3 link font-medium capitalize">
        <Link href={`/product-details/${id}`}>{title}</Link>
      </h4>
      <p className="text-xs  mb-2 line-clamp-2 text-gray-500 link">
        <Link href={`/product-details/${id}`}>{description}</Link>
      </p>
      <div className="mb-5 mt-2 font-bold text-gray-700 flex gap-4">
        <Currency quantity={price} currency="USD" />
        <div className="flex gap-1 items-center">
          <AiFillStar className="text-amber-300" />
          <span>{rating}</span>
        </div>
      </div>
      <button
        className="mt-auto button flex items-center justify-center"
        onClick={addItemToCart}
      >
        <FiShoppingCart className="w-4" />
        <span className="ml-2">Add to Cart</span>
      </button>
    </div>
  );
};

export default Product;
