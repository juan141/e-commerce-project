import Image from "next/image";
import { useDispatch } from "react-redux";
import Currency from "react-currency-formatter";
import { useRouter } from "next/router";
import Skeleton from "react-loading-skeleton";
import { IProduct } from "@/interfaces/Product";
import { addToBasket } from "@/slices/basketSlice";
import { FiShoppingCart } from "react-icons/fi";
import { IoMdArrowBack } from "react-icons/io";
import { AiTwotoneShopping, AiFillStar } from "react-icons/ai";

function ProductDetails({
  id,
  title,
  price,
  description,
  category,
  image,
  rating,
}: IProduct) {
  const router = useRouter();
  const dispatch = useDispatch();
  const addItemToCart = () =>
    dispatch(
      addToBasket({
        id,
        title,
        price,
        description,
        category,
        image,
        qty: 1,
        rating,
        toast: true,
      })
    );

  const buyNow = () => {
    addItemToCart();
    router.push("/cart");
  };

  return (
    <>
      <IoMdArrowBack
        onClick={() => router.push("/")}
        className="w-20 h-8 mt-2 cursor-pointer"
      />
      <div className="heightFix px-6 lg:py-32 md:py-28 py-12 pb-20">
        <div className="max-w-screen-xl flex items-center mx-auto">
          <div className="flex md:flex-row flex-col md:justify-between w-full md:gap-4 gap-8">
            {router.isFallback ? (
              <Skeleton width={400} height={400} />
            ) : (
              <div className="mx-auto">
                <Image
                  src={image}
                  alt=""
                  width={400}
                  height={400}
                  objectFit="contain"
                />
              </div>
            )}
            <div className="flex-grow xl:max-w-2xl lg:max-w-xl  md:max-w-md">
              {router.isFallback ? (
                <Skeleton count={12} />
              ) : (
                <>
                  <h3 className="font-bold xl:text-4xl  lg:text-3xl text-2xl mb-2 capitalize">
                    {title}
                  </h3>
                  <p className="text-amber-300 capitalize mb-4 font-medium">
                    {category}
                  </p>
                  <p className="text-justify text-sm lg:text-base my-6">
                    {description}
                  </p>
                  <div className="flex gap-1 items-center">
                    <AiFillStar className="text-amber-300" />
                    <span className="text-2xl font-semibold text-gray-700">
                      {rating}
                    </span>
                  </div>
                  <p className="text-2xl font-semibold text-gray-700">
                    <Currency quantity={price} currency="USD" />
                  </p>
                  <div className="mt-10 flex xs:flex-row flex-col sm:gap-8 gap-6">
                    <button
                      className="button px-10  py-2 flex items-center justify-center"
                      onClick={addItemToCart}
                    >
                      <FiShoppingCart className="w-4" />
                      <span className="ml-2">Add to Cart</span>
                    </button>
                    <button
                      className=" button-green px-10   py-2 flex items-center justify-center"
                      onClick={buyNow}
                    >
                      <AiTwotoneShopping className="w-4" />
                      <span className="ml-2">Buy Now</span>
                    </button>
                  </div>
                </>
              )}
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

export default ProductDetails;
