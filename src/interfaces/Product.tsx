export interface IProduct {
  id: number;
  title: string;
  description: string;
  price: number;
  rating: number;
  category: string;
  image: string;
  qty?: number;
  border?: boolean;
  toast?: boolean;
}
