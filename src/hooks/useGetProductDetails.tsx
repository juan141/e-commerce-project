import { useState, useEffect } from "react";
import { IProduct } from "../interfaces/Product";

interface Props {
  id: number;
}

const useGetProductDetails = ({ id }: Props) => {
  const [product, setProduct] = useState<IProduct>({} as IProduct);
  const [loading, setLoading] = useState<boolean>(true);

  useEffect(() => {
    fetch("https://dummyjson.com/products/" + id)
      .then((res) => res.json())
      .then((data) => {
        setProduct({
          id: data.id,
          title: data.title,
          description: data.description,
          price: data.price,
          image: data.images[0],
          category: data.category,
          rating: data.rating,
        });
        setLoading(false);
      });
  }, []);

  return {
    product,
    loading,
  };
};

export default useGetProductDetails;
