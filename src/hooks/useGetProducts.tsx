import { useState, useEffect, Dispatch, SetStateAction } from "react";
import { IProduct } from "../interfaces/Product";

interface Props {
  page: number;
  setTotal: Dispatch<SetStateAction<number>>;
}

const useGetProducts = ({ page, setTotal }: Props) => {
  const [products, setProducts] = useState<IProduct[]>([]);
  const [categories, setCategories] = useState<string[]>([]);
  const [loading, setLoading] = useState<boolean>(true);

  const getProducts = () => {
    setLoading(true);
    fetch(`https://dummyjson.com/products?limit=10&skip=${page * 10}`)
      .then((res) => res.json())
      .then((data) => {
        setProducts(
          data?.products?.map((product: any) => ({
            id: product?.id,
            title: product?.title,
            description: product?.description,
            price: product?.price,
            image: product?.images[0],
            rating: product?.rating,
            category: product?.category,
          }))
        );
        setTotal(data?.total);
        const category = data?.products?.map(
          (product: IProduct) => product.category
        );
        setCategories(
          category.filter(
            (item: string, index: number) => category?.indexOf(item) === index
          )
        );
        setLoading(false);
      });
  };

  useEffect(() => {
    getProducts();
  }, [page]);

  return {
    products,
    loading,
    categories,
    getProducts,
  };
};

export default useGetProducts;
