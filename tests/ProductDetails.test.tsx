import ProductDetails from "@/components/ProductDetails";
import "@testing-library/jest-dom";
import { fireEvent, render, screen } from "@testing-library/react";
import { useDispatch } from "react-redux";

jest.mock("react-redux", () => ({
  useDispatch: jest.fn().mockReturnValue(jest.fn()),
}));
const useRouterMock = { push: jest.fn() };

jest.mock("next/router", () => ({
  useRouter: () => useRouterMock,
}));

describe("ProductDetails Component", () => {
  const mockProduct = {
    category: "Test Category",
    description: "This is a test product",
    title: "Test Product",
    image: "https://test-image.jpg",
    price: 19.99,
    id: 1,
    rating: 4.5,
  };
  it("should display product information correctly", () => {
    render(<ProductDetails {...mockProduct} />);
    const titleElement = screen.getByText("Test Product");
    const categoryElement = screen.getByText("Test Category");
    const descriptionElement = screen.getByText("This is a test product");
    const priceElement = screen.getByText("$19.99");
    const ratingElement = screen.getByText("4.5");
    const addToCartButton = screen.getByText("Add to Cart");
    const buyNowButton = screen.getByText("Buy Now");

    expect(titleElement).toBeInTheDocument();
    expect(categoryElement).toBeInTheDocument();
    expect(descriptionElement).toBeInTheDocument();
    expect(priceElement).toBeInTheDocument();
    expect(ratingElement).toBeInTheDocument();
    expect(addToCartButton).toBeInTheDocument();
    expect(buyNowButton).toBeInTheDocument();
  });

  it("should call the addItemToCart function when the 'Add to Cart' button is clicked", () => {
    render(<ProductDetails {...mockProduct} />);
    const addToCartButton = screen.getByText("Add to Cart");
    fireEvent.click(addToCartButton);

    expect(useDispatch).toHaveBeenCalled();
  });

  it("should call the buyNow function and navigate to the cart page when the 'Buy Now' button is clicked", () => {
    render(<ProductDetails {...mockProduct} />);
    const buyNowButton = screen.getByText("Buy Now");
    fireEvent.click(buyNowButton);

    expect(useDispatch).toHaveBeenCalled();

    expect(useRouterMock.push).toHaveBeenCalledWith("/cart");
  });
});
