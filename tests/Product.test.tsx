import Product from "@/components/Product";
import "@testing-library/jest-dom";
import { fireEvent, render, screen } from "@testing-library/react";
import { useDispatch } from "react-redux";

jest.mock("react-redux", () => ({
  useDispatch: jest.fn().mockReturnValue(jest.fn()),
}));
const useRouterMock = { push: jest.fn() };

jest.mock("next/router", () => ({
  useRouter: () => useRouterMock,
}));

describe("Product Component", () => {
  const mockProduct = {
    category: "Test Category",
    description: "This is a test product",
    title: "Test Product",
    image: "https://test-image.jpg",
    price: 19.99,
    id: 1,
    rating: 4.5,
  };
  it("Renders correctly", () => {
    render(<Product {...mockProduct} />);
    const titleElement = screen.getByText("Test Product");
    const descriptionElement = screen.getByText("This is a test product");
    const categoryElement = screen.getByText("Test Category");
    const priceElement = screen.getByText("$19.99");
    const ratingElement = screen.getByText("4.5");

    expect(titleElement).toBeInTheDocument();
    expect(descriptionElement).toBeInTheDocument();
    expect(categoryElement).toBeInTheDocument();
    expect(priceElement).toBeInTheDocument();
    expect(ratingElement).toBeInTheDocument();
  });
  it("should call the addItemToCart function when the 'Add to Cart' button is clicked", () => {
    render(<Product {...mockProduct} />);
    const addToCartButton = screen.getByText("Add to Cart");
    fireEvent.click(addToCartButton);

    expect(useDispatch).toHaveBeenCalled();
  });
  it("should navigate to the product details page when the product image is clicked", () => {
    render(<Product {...mockProduct} />);
    const productImage = screen.getByAltText("product-image");
    fireEvent.click(productImage);

    expect(useRouterMock.push).toHaveBeenCalled();
  });
});
