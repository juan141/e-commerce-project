import Search from "@/components/Search";
import "@testing-library/jest-dom";
import { fireEvent, render, screen, waitFor } from "@testing-library/react";

(global as any).fetch = jest.fn(() =>
  Promise.resolve({
    json: () =>
      Promise.resolve({
        products: [
          {
            id: 1,
            title: "Product 1",
            description: "Description 1",
            price: 10.99,
            images: ["image1.jpg"],
            category: "Category A",
            rating: 4.5,
          },
        ],
      }),
  })
);

describe("Search Component", () => {
  const props = {
    setCustomProducts: jest.fn(),
    setCustomCategories: jest.fn(),
    setCustomLoading: jest.fn(),
    setFilterSelected: jest.fn(),
  };
  it("Renders correctly", () => {
    render(<Search {...props} />);
    expect(screen.getByPlaceholderText("Search a product")).toBeInTheDocument();
  });
  it("should update search input value when typing", () => {
    render(<Search {...props} />);

    const searchInput = screen.getByPlaceholderText("Search a product");
    fireEvent.change(searchInput, { target: { value: "test search" } });

    expect((searchInput as any).value).toBe("test search");
  });
  it("should call the search function when Enter is pressed and the search input is not empty", async () => {
    const setCustomProducts = jest.fn();
    const setCustomCategories = jest.fn();
    const setCustomLoading = jest.fn();
    const setFilterSelected = jest.fn();

    render(
      <Search
        setCustomProducts={setCustomProducts}
        setCustomCategories={setCustomCategories}
        setCustomLoading={setCustomLoading}
        setFilterSelected={setFilterSelected}
      />
    );

    const searchInput = screen.getByPlaceholderText("Search a product");
    fireEvent.change(searchInput, { target: { value: "test search" } });
    fireEvent.keyDown(searchInput, { key: "Enter", code: "Enter" });

    await waitFor(() => {
      expect(setCustomProducts).toHaveBeenCalled();
      expect(setCustomCategories).toHaveBeenCalled();
      expect(setCustomLoading).toHaveBeenCalledWith(true);
      expect(setFilterSelected).toHaveBeenCalledWith("test search");
    });
  });
});
