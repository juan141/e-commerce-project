import Header from "@/components/Header";
import "@testing-library/jest-dom";
import { fireEvent, render, screen } from "@testing-library/react";
import { useRouter } from "next/router";
import { useSelector } from "react-redux";

const mockItems = [
  {
    id: 1,
    title: "Product 1",
    price: 19.99,
    description: "This is product 1",
    category: "Category A",
    image: "product1.jpg",
    qty: 2,
    rating: 4.5,
  },
  {
    id: 2,
    title: "Product 2",
    price: 29.99,
    description: "This is product 2",
    category: "Category B",
    image: "product2.jpg",
    qty: 3,
    rating: 4.2,
  },
];

jest.mock("next/router", () => ({
  useRouter: jest.fn(),
}));

jest.mock("react-redux", () => ({
  useSelector: jest.fn().mockReturnValue(jest.fn()),
}));

const pushMock = jest.fn();

(useRouter as any).mockReturnValue({
  query: {},
  push: pushMock,
});
describe("Header Component", () => {
  beforeEach(() => {
    (useSelector as any).mockReturnValue(mockItems);
  });
  const props = {
    setCustomProducts: jest.fn(),
    setCustomCategories: jest.fn(),
    setCustomLoading: jest.fn(),
    setFilterSelected: jest.fn(),
  };
  it("Renders correctly", () => {
    render(<Header {...props} />);
    expect(screen.getByText("E-Commerce")).toBeInTheDocument();
  });
  it("should display the logo correctly and navigate to the homepage on click", () => {
    render(<Header {...props} />);
    const logo = screen.getByText("E-Commerce");
    fireEvent.click(logo);
    expect(logo).toBeInTheDocument();
    expect(window.location.pathname).toBe("/");
  });
  it("should navigate to the cart page when the cart icon is clicked", () => {
    render(<Header {...props} />);
    const cartIcon = screen.getByTestId("cart-icon");
    fireEvent.click(cartIcon);
    expect(cartIcon).toBeTruthy();
  });
  it("should display the cart icon with the correct number of items", () => {
    render(<Header {...props} />);
    const cartIcon = screen.getByTestId("cart-icon");
    const itemCount = screen.getByTestId("cart-item-count");
    expect(cartIcon).toBeInTheDocument();
    expect(itemCount.textContent).toBe("2"); // Mocked number of items in the cart
  });
});
