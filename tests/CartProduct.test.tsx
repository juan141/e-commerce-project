import CartProduct from "@/components/CartProduct";
import "@testing-library/jest-dom";
import { fireEvent, render, screen } from "@testing-library/react";

jest.mock("react-redux", () => ({
  useDispatch: jest.fn().mockReturnValue(jest.fn()),
}));

jest.mock("next/router", () => ({
  useRouter: jest.fn(),
}));

describe("CartProduct Component", () => {
  const mockProduct = {
    id: 1,
    title: "Test Product",
    price: 19.99,
    description: "This is a test product",
    category: "Test Category",
    image: "https://test-image.jpg",
    qty: 3,
    border: true,
    disabled: false,
    rating: 4.5,
  };
  it("Renders correctly", () => {
    render(<CartProduct {...mockProduct} />);
    const titleElement = screen.getByText(mockProduct.title);
    const descriptionElement = screen.getByText(mockProduct.description);
    const totalElement = screen.getByText("$59.97");

    expect(titleElement).toBeInTheDocument();
    expect(descriptionElement).toBeInTheDocument();
    expect(totalElement).toBeInTheDocument();
  });
  it("should call the removeItemFromCart function when the 'Remove' button is clicked", () => {
    render(<CartProduct {...mockProduct} />);

    const removeButton = screen.getByText("Remove");
    fireEvent.click(removeButton);

    expect(removeButton).toBeTruthy();
  });
  it("should click the '+' button", () => {
    render(<CartProduct {...mockProduct} />);

    const plusButton = screen.getByTestId("plus-button");
    fireEvent.click(plusButton);

    expect(plusButton).toBeTruthy();
  });
  it("should click the '-' button", () => {
    render(<CartProduct {...mockProduct} />);

    const minusButton = screen.getByTestId("minus-button");
    fireEvent.click(minusButton);

    expect(minusButton).toBeTruthy();
  });
});
