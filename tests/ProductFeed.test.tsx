import ProductFeed from "@/components/ProductFeed";
import "@testing-library/jest-dom";
import { fireEvent, render, screen } from "@testing-library/react";

jest.mock("next/router", () => ({
  useRouter: () => jest.fn(),
}));

jest.mock("react-redux", () => ({
  useDispatch: jest.fn().mockReturnValue(jest.fn()),
}));

describe("ProductFeed Component", () => {
  const props = {
    products: [
      {
        category: "Category A",
        description: "This is a test product",
        title: "Product 1",
        image: "https://test-image.jpg",
        price: 19.99,
        id: 1,
        rating: 4.5,
      },
    ],
    categories: ["Category A", "Category B", "Category C"],
    filterSelected: "",
    setFilterSelected: jest.fn(),
    getProducts: jest.fn(),
  };
  it("Renders correctly", () => {
    render(<ProductFeed {...props} />);
    expect(screen.getByText("All")).toBeInTheDocument();
    expect(screen.getByText("Category B")).toBeInTheDocument();
    expect(screen.getByText("Category C")).toBeInTheDocument();
    expect(screen.getByText(props.products[0].title)).toBeInTheDocument();
  });

  it("should filter products when a category B is clicked", () => {
    render(<ProductFeed {...props} />);

    const categoryBButton = screen.getByText("Category B");
    fireEvent.click(categoryBButton);

    expect(categoryBButton).toBeTruthy();
  });
  it("should filter products when a category C is clicked", () => {
    render(<ProductFeed {...props} />);

    const categoryCButton = screen.getAllByText("Category C");
    fireEvent.click(categoryCButton[0]);

    expect(categoryCButton).toBeTruthy();
  });
  it("should display all products when 'All' is clicked", () => {
    render(<ProductFeed {...props} />);

    const allButton = screen.getByText("All");
    fireEvent.click(allButton);

    expect(screen.getByText("Product 1")).toBeInTheDocument();
  });

  it("should filter products when a category A is clicked", () => {
    render(<ProductFeed {...props} />);

    const categoryAButton = screen.getAllByText("Category A");
    fireEvent.click(categoryAButton[0]);

    expect(categoryAButton).toBeTruthy();
  });
});
