import Pagination from "@/components/Pagination";
import "@testing-library/jest-dom";
import { fireEvent, render, screen } from "@testing-library/react";

describe("Pagination Component", () => {
  const props = { page: 0, total: 100, setPage: jest.fn() };
  it("Renders correctly", () => {
    render(<Pagination {...props} />);
    expect(screen.getByText("Prev")).toBeInTheDocument();
    expect(screen.getByText("Prev")).toBeInTheDocument();
    expect(screen.getByText("1 of 10")).toBeInTheDocument();
  });
  it("Should click the prev button", () => {
    render(<Pagination {...props} />);
    const prevBtn = screen.getByTestId("prev-btn");
    fireEvent.click(prevBtn);
    expect(prevBtn).toBeTruthy();
  });
  it("Should click the next button", () => {
    render(<Pagination {...props} />);
    const nextBtn = screen.getByTestId("next-btn");
    fireEvent.click(nextBtn);
    expect(nextBtn).toBeTruthy();
  });
});
